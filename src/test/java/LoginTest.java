import org.testng.annotations.Test;
import pages.LoginPage;

public class LoginTest extends SeleniumBaseTest{

    @Test
    public void correctLoginTest() {
        new LoginPage(driver)
                .typeEmail(config.getTestUser())
                .typePassword(config.getTestUserPassword())
                .submitLogin()
                    .assertUserName("Welcome\ntest@test.com");
    }
}
