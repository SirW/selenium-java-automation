import org.testng.annotations.Test;
import pages.LoginPage;

public class AddProcessWithFailure extends SeleniumBaseTest {

    @Test
    public void addProcessWithFailure() {
        new LoginPage(driver)
                .typeEmail(config.getTestUser())
                .typePassword(config.getTestUserPassword())
                .submitLogin()
                .goToProcessesPage()
                .createProcess()
                .submitProcessWithFailure()
                    .assertCreationError(config.getNoNameErrorText())
                .submitProcessName("a")
                .submitProcessWithFailure()
                    .assertCreationError(config.getNameSizeErrorText())
                .submitProcessName("0123456789012345678901234567890")
                .submitProcessWithFailure()
                    .assertCreationError(config.getNameSizeErrorText());
    }
}
