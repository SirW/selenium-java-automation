import org.testng.annotations.Test;
import pages.LoginPage;

public class NavigationTest extends SeleniumBaseTest{

    @Test
    public void navigationTest() {
        new LoginPage(driver)
                .typeEmail(config.getTestUser())
                .typePassword(config.getTestUserPassword())
                .submitLogin()
                    .assertUrl(config.getAppUrl())
                .goToProcessesPage()
                    .assertUrl(config.getProcessesUrl())
                .goToCharacteristicsPage()
                    .assertUrl(config.getCharacteristicsUrl())
                .goToProcessesPage()
                    .assertUrl(config.getProcessesUrl())
                .goToHomePage()
                    .assertUrl(config.getAppUrl())
                .goToCharacteristicsPage()
                    .assertUrl(config.getCharacteristicsUrl())
                .goToHomePage()
                    .assertUrl(config.getAppUrl());
    }
}
