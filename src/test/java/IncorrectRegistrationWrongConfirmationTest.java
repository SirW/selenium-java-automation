import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class IncorrectRegistrationWrongConfirmationTest extends SeleniumBaseTest{

    @Test
    public void incorrectPasswordConfirmationTest() {

        UUID uuid = UUID.randomUUID();
        String email = uuid + "@test.com";

        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail(email)
                .typePassword(config.getTestUserPassword())
                .submitRegisterWithFailure()
                    .assertEmailConfirmationError();
    }
}
