import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class IncorrectRegistrationPasswordFormat extends SeleniumBaseTest {

    @DataProvider
    public Object[][] passwordsErrors() {
        return new Object[][] {
                {"Test1", "The Password must be at least 6 and at max 100 characters long."},
                {"Test11", "Passwords must have at least one non alphanumeric character."},
                {"test1!", "Passwords must have at least one uppercase ('A'-'Z')."}
        };
    }

    @Test(dataProvider = "passwordsErrors")
    public void registrationPasswordFormatErrorTest(String password, String expectedError) {
        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail(config.getTestUser())
                .typePassword(password)
                .typeConfirmPassword(password)
                .submitRegisterWithFailure()
                    .assertList(expectedError);
    }

}
