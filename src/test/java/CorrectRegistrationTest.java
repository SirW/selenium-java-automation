import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class CorrectRegistrationTest extends SeleniumBaseTest {

    @Test
    public void correctRegistrationTest() {

        UUID uuid = UUID.randomUUID();
        String email = uuid + "@test.com";

        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail(email)
                .typePassword(config.getTestUserPassword())
                .typeConfirmPassword(config.getTestUserPassword())
                .submitRegister()
                    .assertUserName("Welcome\n" + email);
    }
}
