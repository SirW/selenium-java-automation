package config;

import java.io.InputStream;
import java.util.Properties;

public class Config {
    private Properties properties;

    private Properties getProperties() {
        Properties prop = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            prop.load(inputStream);
        } catch (Exception e) {
            throw new RuntimeException("Cannot load properties file " + e);
        }
        return prop;
    }

    public Config() {
        properties = getProperties();
    }

    public String getAppUrl() {
        return properties.getProperty("app.url");
    }

    public String getTestUser() {
        return properties.getProperty("test.user");
    }

    public String getTestUserPassword() {
        return properties.getProperty("test.user.password");
    }

    public String getProcessesUrl() {
        return properties.getProperty("processes.url");
    }

    public String getCharacteristicsUrl() {
        return properties.getProperty("characteristics.url");
    }

    public String getNoNameErrorText() {
        return properties.getProperty("process.name.required");
    }

    public String getNameSizeErrorText() {
        return properties.getProperty("process.name.wrong.size");
    }
}
