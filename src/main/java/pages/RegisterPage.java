package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class RegisterPage {
    protected WebDriver driver;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPasswordTxt;

    @FindBy(id = "Password-error")
    private WebElement passError;

    @FindBy(css = "button[type=submit]")
    private WebElement registerButton;

    @FindBy(id = "Password-error")
    private WebElement passwordError;

    @FindBy(id = "ConfirmPassword-error")
    private WebElement confirmPasswordError;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> registerErrors;

    public RegisterPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);

        return this;
    }

    public RegisterPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);

        return this;
    }

    public RegisterPage typeConfirmPassword(String password) {
        confirmPasswordTxt.clear();
        confirmPasswordTxt.sendKeys(password);

        return this;
    }

    public HomePage submitRegister() {
        registerButton.click();

        return new HomePage(driver);
    }

    public RegisterPage submitRegisterWithFailure() {
        registerButton.click();

        return this;
    }

    public String getEmail() {
        return emailTxt.getText();
    }

    public RegisterPage assertList(String expected) {
        boolean errorExists = false;

        errorExists = registerErrors
                .stream()
                .anyMatch(registerError -> registerError.getText().equals(expected));
        Assert.assertTrue(errorExists);

        return this;
    }

    public RegisterPage assertEmailConfirmationError() {
        Assert.assertEquals(confirmPasswordError.getText(), "The password and confirmation password do not match.");

        return this;
    }
}
