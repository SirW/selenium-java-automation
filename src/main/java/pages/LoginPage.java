package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class LoginPage {
    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;

    @FindBy(css = "a[href*=Register")
    private WebElement registerBtn;

    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);

        return this;
    }

    public LoginPage typePassword(String password) {
        passTxt.clear();
        passTxt.sendKeys(password);

        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);

    }

    public LoginPage submitLoginWithFailure() {
        loginBtn.click();

        return this;
    }

    public RegisterPage goToRegisterPage() {
        registerBtn.click();

        return new RegisterPage(driver);
    }

    public LoginPage assertList(String expected) {
        boolean errorExists = false;

        errorExists = loginErrors
                .stream()
                .anyMatch(loginError -> loginError.getText().equals(expected));
        Assert.assertTrue(errorExists);

        return this;
    }
}
