package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateProcessPage {
    protected WebDriver driver;

    public CreateProcessPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Name")
    private WebElement processName;

    @FindBy(css = "input[type=submit]")
    private WebElement submitProcess;

    @FindBy(css = ".form-group span")
    private WebElement processNameError;

    public CreateProcessPage submitProcessName(String name) {
        processName.clear();
        processName.sendKeys(name);

        return this;
    }

    public ProcessesPage submitProcess() {
        submitProcess.click();

        return new ProcessesPage(driver);
    }

    public CreateProcessPage submitProcessWithFailure() {
        submitProcess.click();

        return this;
    }

    public CreateProcessPage assertCreationError(String expected) {
        Assert.assertEquals(processNameError.getText(), expected);

        return this;
    }
}
