package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ProcessesPage {
    protected WebDriver driver;

    public ProcessesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "a[href$=Create]")
    private WebElement createProcess;

    @FindBy(css = "a[href$=Projects]")
    private WebElement processesNav;

    @FindBy(css = "a[href$=Characteristics")
    private WebElement characteristicsNav;

    @FindBy(xpath = "//a[contains(text(), 'Dashboard')]")
    private WebElement dashboardNav;

    @FindBy(css = ".menu-home")
    private WebElement sidebarHomeMenu;

    @FindBy(css = ".menu-workspace")
    private WebElement sidebarWorkspaceMenu;

    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));

        return parent.getAttribute("class").contains("active");
    }

    public HomePage goToHomePage() {

        if(!isParentExpanded(sidebarHomeMenu)) {
            sidebarHomeMenu.click();
        }

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(dashboardNav));
        dashboardNav.click();

        return new HomePage(driver);
    }

    public CharacteristicsPage goToCharacteristicsPage() {

        if(!isParentExpanded(sidebarWorkspaceMenu)) {
            sidebarWorkspaceMenu.click();
        }

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(characteristicsNav));
        characteristicsNav.click();

        return new CharacteristicsPage(driver);
    }

    public CreateProcessPage createProcess() {
        createProcess.click();

        return new CreateProcessPage(driver);
    }

    public ProcessesPage assertUrl(String expected) {
        Assert.assertEquals(driver.getCurrentUrl(), expected);

        return this;
    }
}
